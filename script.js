// User
let idUser = window.location.search.substring(4)

// récupére toute la table depuis la BDD
function getGroups() {
    fetch('getUserGroup.php', {
        method: 'POST',
        headers: {
            Accept: 'application/json'
        },
        body: JSON.stringify({idUser: idUser})
    })
        .then(r => {
            if (r.ok) {
                return r.json()
            } else {
                throw new Error('Erreur serveur', {cause: r})
            }
        })
        .then(posts => {
            const user = posts[0].USERNAME
            console.log(user)
            posts.forEach((name) => {
                console.log(name.GROUP_NAME)
            })
        })
        .catch(e => {
            console.error('Une erreur est survenue', e)
        });

}// récupére toute la table depuis la BDD
function getFriends() {
    fetch('getUserFriend.php', {
        method: 'POST',
        headers: {
            Accept: 'application/json'
        },
        body: JSON.stringify({idUser: idUser})
    })
        .then(r => {
            if (r.ok) {
                return r.json()
            } else {
                throw new Error('Erreur serveur', {cause: r})
            }
        })
        .then(posts => {
            posts.forEach((friend) => {
                console.log(friend)
            })
        })
        .catch(e => {
            console.error('Une erreur est survenue', e)
        });

}

function addFriend(bool) {

    const idFriend = 2

    const url = bool === 1 ? 'addFriend.php' : 'removeFriend.php';
    fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({idUser: idUser, idFriend: idFriend})
    })
        .then(r => {
            return r.text();
        })
        .then(posts => {
            console.log(posts);
        })
        .catch(e => {
            console.error('Une erreur est survenue', e)
        });
}

function joinGroup(bool) {

    const idGroup = 3;
    // const idUser = 456;
    const isAdmin = 1;

    const url = bool === 1 ? 'joinGroup.php' : 'leaveGroup.php';

    fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({idGroup: idGroup, idUser: idUser, isAdmin: isAdmin})
    })
        .then(r => {
            return r.text()
        })
        .then(posts => {
            console.log(posts);
        })
        .catch(e => {
            console.error('Une erreur est survenue', e)
        })
}

function reset() {
    url = 'fillTable.php';
    fetch(url, {
        method: 'GET'
    })
        .then(r => {
            if (r.ok) {
                return r.text()
            } else {
                throw new Error('Erreur serveur', {cause: r})
            }
        })
        .then(posts => {
            console.log(posts)
        })
        .catch(e => {
            console.error('Une erreur est survenue', e)
        })
}
