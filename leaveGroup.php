<?php

include('connect_db.php');

$mysqli = bdd::connectDB();

$json = json_decode(file_get_contents('php://input'));

bdd::leaveGroup($mysqli, $json->idGroup, $json->idUser);

$mysqli->close();