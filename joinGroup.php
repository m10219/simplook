<?php

include('connect_db.php');

$mysqli = bdd::connectDB();

$json = json_decode(file_get_contents('php://input'));

bdd::joinGroup($mysqli, $json->idGroup, $json->idUser, (isset($json->isAdmin) ? $json->isAdmin : 0));

$mysqli->close();