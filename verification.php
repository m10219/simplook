<?php
session_start();
if(isset($_POST['username']) && isset($_POST['password']))
{
    // connexion à la base de données
    include('connect_db.php');

    $mysqli = bdd::connectDB();

    // on applique les deux fonctions mysqli_real_escape_string et htmlspecialchars
    // pour éliminer toute attaque de type injection SQL et XSS
    $username = mysqli_real_escape_string($mysqli,htmlspecialchars($_POST['username']));
    $password = mysqli_real_escape_string($mysqli,htmlspecialchars($_POST['password']));

    if($username !== "" && $password !== "")
    {
        $requete = "SELECT count(*), ID_USER FROM users where 
 USERNAME = '".$username."' and PASSWORD = '".$password."' ";
//        echo $requete; die;
        $exec_requete = $mysqli->query($requete);
        $reponse = $exec_requete->fetch_array(MYSQLI_ASSOC);
//        var_dump($reponse['ID_USER']); die;
        $count = $reponse['count(*)'];
        if($count!=0) // nom d'utilisateur et mot de passe correctes
        {
            $_SESSION['username'] = $username;
            header("Location: index.phtml?id=".$reponse['ID_USER']);
        }
        else
        {
            header('Location: login.phtml?erreur=1'); // utilisateur ou mot de passe incorrect
        }
    }
    else
    {
        header('Location: login.phtml?erreur=2'); // utilisateur ou mot de passe vide
    }
}
else
{
    header('Location: login.phtml');
}
$mysqli->close();; // fermer la connexion
?>