<?php

class bdd
{
    public static function connectDB()
    {
        $mysqli = new mysqli("localhost", "root", "", "simplook", "3306")
        or die("Impossible de se connecter : " . mysqli_error());
        return $mysqli;
    }

    public static function makeUsers($mysqli)
    {
        $usersALL = json_decode(file_get_contents('fake_user.json'));
        $users = array_slice($usersALL, 0, 20);

        $groups = (explode(',', file_get_contents('group_array.txt')));

        $cleanTable = "TRUNCATE table users;";
        if ($mysqli->query($cleanTable)) echo "Table videe\n";
        $cleanGroup = "TRUNCATE table groups";
        if ($mysqli->query($cleanGroup)) echo "Groupe videe\n";

        foreach ($users as $user) {

            $sql = "INSERT INTO `users`(`USERNAME`, `EMAIL`,`PASSWORD`,`AVATAR`)
                    VALUES ('$user->username', '$user->email', '$user->password', '" . preg_replace('/\//', '-', $user->picture) . "') \n";

            if ($mysqli->query($sql)) {
            } else {
                echo "Error: " . $sql . "<br>" . mysqli_error($mysqli);
            }
        }
        echo "Ami OK\n";

        foreach ($groups as $group) {
            $sql = "INSERT INTO `groups`(`GROUP_NAME`) VALUES ('$group')";
            if ($mysqli->query($sql)) {
            }
        }
        echo "Groupe OK\n";
    }

    public static function addFriend($mysqli, $idUser, $idFriend)
    {
        $check = "SELECT *
                  FROM friends
                  WHERE ID_FRIEND = $idFriend AND ID_USER = $idUser";
        if($mysqli->query($check)->fetch_row()) {
            echo "Vous êtes déjà amis !";
            return;
        }


        $sql = "INSERT INTO `friends` VALUES ('$idUser','$idFriend')";
        if ($mysqli->query($sql)) {
            echo "Ami ajouté !";
        }
    }

    public static function removeFriend($mysqli, $idUser, $idFriend)
    {
        $sql = "DELETE FROM friends WHERE ID_USER = $idUser AND ID_FRIEND = $idFriend";
        if ($mysqli->query($sql)) {
            echo "Ami supprimé !";
        }
    }

    public static function joinGroup($mysqli, $idGroup, $idUser, $isAdmin = 0)
    {
        $check = "SELECT * FROM group_members
                  WHERE ID_USER = $idUser AND ID_GROUP = $idGroup";
        if ($mysqli->query($check)->fetch_row()) {
            echo "Tu es déjà dans ce groupe !";
            return;
        }

        $sql = "INSERT INTO group_members (ID_GROUP, ID_USER, IS_ADMIN) VALUES ($idGroup,$idUser,$isAdmin)";
        if ($mysqli->query($sql)) {
            echo "Tu as rejoint le groupe !";
        }
    }

    public static function leaveGroup($mysqli, $idGroup, $idUser)
    {
        $sql = "DELETE FROM group_members WHERE ID_GROUP = $idGroup AND ID_USER = $idUser";
        if ($mysqli->query($sql)) {
            echo "Tu as quitté le groupe !";
        }
    }

    public static function getUserGroup($mysqli, $idUser)
    {
        $sql = "SELECT u.ID_USER, USERNAME, g.ID_GROUP, GROUP_NAME
        FROM users u
        LEFT JOIN group_members gm on u.ID_USER = gm.ID_USER
        LEFT JOIN `groups` g on gm.ID_GROUP = g.ID_GROUP
        WHERE u.ID_USER = $idUser
        ";

        return $mysqli->query($sql)->fetch_all(MYSQLI_ASSOC);
    }

    public static function getUserFriend($mysqli, $idUser)
    {
        $sql2 = "SELECT ID_FRIEND
                FROM friends
                WHERE ID_USER = $idUser
                ";
        $friendID = $mysqli->query($sql2)->fetch_all(MYSQLI_ASSOC);

        $friends = [];
        foreach ($friendID as $id) {

            $sql = "SELECT USERNAME as FRIEND
                FROM users
                WHERE ID_USER = ".$id['ID_FRIEND'];
            $friendName = $mysqli->query($sql)->fetch_all(MYSQLI_ASSOC);
            $friends [] = $friendName[0]["FRIEND"];
        }
        return $friends;
    }
}
