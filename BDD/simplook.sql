-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : Dim 30 oct. 2022 à 14:38
-- Version du serveur :  5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `simplook`
--

-- --------------------------------------------------------

--
-- Structure de la table `friends`
--

DROP TABLE IF EXISTS `friends`;
CREATE TABLE IF NOT EXISTS `friends` (
  `ID_USER` int(4) NOT NULL,
  `ID_FRIEND` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `friends`
--

INSERT INTO `friends` (`ID_USER`, `ID_FRIEND`) VALUES
(1, 16),
(1, 9),
(1, 5);

-- --------------------------------------------------------

--
-- Structure de la table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `ID_GROUP` int(4) NOT NULL AUTO_INCREMENT,
  `GROUP_NAME` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`ID_GROUP`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `groups`
--

INSERT INTO `groups` (`ID_GROUP`, `GROUP_NAME`) VALUES
(1, 'Trouver un emploi'),
(2, 'Astuces de clochard'),
(3, 'Astuces de Mac Gyver'),
(4, 'Trucs chiants qui arrivent'),
(5, 'Les open class room'),
(6, 'Simplon'),
(7, 'Developpeur Noob');

-- --------------------------------------------------------

--
-- Structure de la table `group_members`
--

DROP TABLE IF EXISTS `group_members`;
CREATE TABLE IF NOT EXISTS `group_members` (
  `ID_GROUP` int(4) NOT NULL,
  `ID_USER` int(4) NOT NULL,
  `IS_ADMIN` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `group_members`
--

INSERT INTO `group_members` (`ID_GROUP`, `ID_USER`, `IS_ADMIN`) VALUES
(4, 1, 1),
(2, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `ID_USER` int(4) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `EMAIL` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PASSWORD` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AVATAR` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID_USER`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`ID_USER`, `USERNAME`, `EMAIL`, `PASSWORD`, `AVATAR`) VALUES
(1, 'goldenkoala410', 'melissa.fleming@example.com', 'sick', 'women-01.png'),
(2, 'smallbird985', 'christoffer.christiansen@example.com', 'samuel', 'men-01.png'),
(3, 'brownfish540', 'valtteri.pulkkinen@example.com', 'peepee', 'men-02.png'),
(4, 'bigelephant503more27', 'todd.beck@example.com', 'rrrrr', 'men-03.png'),
(5, 'beautifulkoala36cool', 'kayla.hall@example.com', 'lickit', 'women-02.png'),
(6, 'silverelephant404yes', 'jimmie.simmons@example.com', 'stang', 'men-04.jpg'),
(7, 'goldenelephant510zoo', 'benedikt.hein@example.com', 'katie1', 'men-05.jpeg'),
(8, 'silvergorilla96mybad', 'noah.smith@example.com', 'kane', 'men-06.jpeg'),
(9, 'redbear614', 'noah.dupont@example.com', 'adidas', 'men-07.jpeg'),
(10, 'tinybird877', 'nerea.mendez@example.com', '4567', 'women-03.jpeg'),
(11, 'yellowwolf551', 'arnold.gardner@example.com', 'black', 'men-08.jpeg'),
(12, 'tinypanda172', 'julia.cano@example.com', 'cumshot', 'women-04.jpg'),
(13, 'lazysnake906', 'annika.schulte@example.com', 'bullseye', 'women-05.jpg'),
(14, 'beautifulbutterfly981', 'justin.harcourt@example.com', 'summit', 'men-09.jpg'),
(15, 'ticklishswan854', 'noah.bonnet@example.com', 'gateway', 'men-10.jpg'),
(16, 'beautifuldog542', 'eemil.neva@example.com', 'mustang', 'men-11.jpg'),
(17, 'bluegorilla445', 'leevi.wiitala@example.com', 'europe', 'men-12.jpg'),
(18, 'tinyfish204', 'alma.thomsen@example.com', 'moomoo', 'women-06.jpg'),
(19, 'organicrabbit832', 'mia.li@example.com', 'christa', 'women-07.jpg'),
(20, 'ticklishbird503', 'james.warren@example.com', 'coconut', 'men-13.jpg');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
