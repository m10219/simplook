<?php

include('connect_db.php');

$mysqli = bdd::connectDB();

$json = json_decode(file_get_contents('php://input'));

$groups = bdd::getUserGroup($mysqli, $json->idUser);

echo json_encode($groups);

$mysqli->close();