<?php

include('connect_db.php');

$mysqli = bdd::connectDB();

$json = json_decode(file_get_contents('php://input'));

$friends = bdd::getUserFriend($mysqli, $json->idUser);

echo json_encode($friends);

$mysqli->close();